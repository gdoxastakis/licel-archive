import bisect
import codecs
import copy
import glob
import logging
import os
import re

import datetime
import matplotlib as mpl
import numpy as np
import pytz
from matplotlib import pyplot as plt

from atmospheric_lidar import licel, raymetrics

logger = logging.getLogger(__name__)


class LicelArchive(object):

    measurement_classes = {'default': licel.LicelLidarMeasurement ,
                           'scanning': raymetrics.ScanningLidarMeasurement,
                           'vertical': raymetrics.VerticalLidarMeasurement,}

    @staticmethod
    def filenames_to_date_short(filenames):
        """ Convert the filename list to a datetime list.

        This method works with **short** format of Licel filenames (e.g. RM12A2810.341)

        Parameters
        ----------
        filenames : list
           A list of filenames to convert filename to convert.

        Returns
        -------
        datetimes : list
           A list of datetime object corresponding to the filenames.

        Notes
        -----
        Some ideas from here: https://stackoverflow.com/a/14163523
        """
        regex_str = r'\D{1,2}(?P<year>\d\d)(?P<month>[0-9,A,B,C])(?P<day>\d\d)(?P<hour>\d\d).(?P<minute>\d\d)(?P<second>\d)'
        pattern = re.compile(regex_str)

        datetimes = []
        for filename in filenames:
            match = pattern.match(filename)

            if not match:
                raise IOError("Filename {0} does not match short Licel format pattern.".format(filename))

            groups = match.groupdict()
            year = int(groups['year']) + 2000  # Convert to full year eg. 18->2018
            month = int(groups['month'], 16)  # Hexadeciaml format
            day = int(groups['day'])
            hour = int(groups['hour'])
            minute = int(groups['minute'])
            second = int(groups['second']) * 10 + 5  # Convert to approximate seconds e.g 1 to 15, 2 to 25 etc.

            d = datetime.datetime(year, month, day, hour, minute, second)
            datetimes.append(d)

        return datetimes

    @staticmethod
    def filenames_to_date_long(filenames):
        """ Convert the filename list to a datetime list.

        This method works with **long** format of Licel filenames (e.g. RM12A2810.341123)

        Parameters
        ----------
        filenames : list
           A list of filenames to convert filename to convert.

        Returns
        -------
        datetimes : list
           A list of datetime object corresponding to the filenames.

        Notes
        -----
        Some ideas from here: https://stackoverflow.com/a/14163523
        """
        regex_str = r'\D{1,2}(?P<year>\d\d)(?P<month>[0-9,A,B,C])(?P<day>\d\d)(?P<hour>\d\d).(?P<minute>\d\d)(?P<second>\d\d)(?P<microsecond>\d\d)'
        pattern = re.compile(regex_str)

        datetimes = []
        for filename in filenames:
            match = pattern.match(filename)

            if not match:
                raise IOError("Filename {0} does not match long Licel format pattern.".format(filename))

            groups = match.groupdict()
            year = int(groups['year']) + 2000  # Convert to full year eg. 18->2018
            month = int(groups['month'], 16)  # Hexadeciaml format
            day = int(groups['day'])
            hour = int(groups['hour'])
            minute = int(groups['minute'])
            second = int(groups['second'])
            microsecond = int(groups['microsecond']) * 10000  # Licel files give two decimal places of seconds.

            d = datetime.datetime(year, month, day, hour, minute, second, microsecond)
            datetimes.append(d)

        return datetimes

    def _find_files_in_archive(self):
        """ Populate the file list with all files found in the archive.

        The method also reads the approximate start time of each file, based on the filename.
        """
        search_pattern = os.path.join(self.base_path, self.file_pattern)

        full_paths = glob.glob(search_pattern)
        file_names = [os.path.basename(path) for path in full_paths]

        if len(full_paths) > 0:
            # Sort both based on file_names, see https://stackoverflow.com/a/9764364
            file_names, full_paths = zip(*sorted(zip(file_names, full_paths)))

            relative_paths = [os.path.relpath(path, self.base_path) for path in full_paths]

            if self.short_filenames:
                datetimes = self.filenames_to_date_short(file_names)
            else:
                datetimes = self.filenames_to_date_long(file_names)

            self.relative_paths = relative_paths
            self.stop_time_approximate = datetimes
        else:
            logger.warning("No files matching file_pattern were found.")

        self.full_paths = full_paths
        self.file_names = file_names

    def plot_channel(self, channel_name, figsize=(12, 5), vmin=None, vmax=None, minimum_length=8, date_labels=False,
                     zoom=[0, 12000, 0, None]):
        """ Plot a specific channel for a specif interval. """

        # Customize colorbar
        jet = copy.copy(mpl.cm.jet)
        jet.set_over('white')
        jet.set_under('black')
        jet.set_bad('black')

        fig = plt.figure(figsize=figsize)
        ax = plt.subplot(111)
        ax.set_facecolor((0.5, 0.5, 0.5))

        is_first = True

        for dataset in self.loaded_datasets:
            m = dataset['measurement']

            if (channel_name in m.channels.keys()) and (len(m.files) >= minimum_length):
                if is_first:
                    add_colorbar = True
                    is_first = False
                else:
                    add_colorbar = False

                c = m.channels[channel_name]
                c.draw_plot_new(ax, add_colorbar=add_colorbar, vmin=vmin, vmax=vmax, cmap=jet, date_labels=date_labels,
                                zoom=zoom)

        plt.tight_layout()
        plt.draw()
        plt.show()

        return fig, ax


class RawLicelArchive(LicelArchive):

    def __init__(self, base_path, file_pattern, file_version='default', short_filenames=True):
        """ A Licel archive without any Datalog to describe indiviudal datasets.
        Parameters
        ----------
        base_path : str
           The base path of the archive.
        file_pattern : str
           A glob pattern for finding licel files in the archive (e.g. */*/*)
        file_version : str
           Version of licel files used. 'default': classic licel file, 'scanning': Scanning file with extra line.
        short_filenames : bool
           If True, short filenames are used (e.g. RM10A0210.351). If False, long filenames are used.
        """
        self.base_path = base_path
        self.file_pattern = file_pattern
        self.short_filenames = short_filenames
        self.file_version = file_version
        self.measurement_class = self.measurement_classes[file_version]

        self._find_files_in_archive()
        self._get_exact_info()
        self.gaps = self._calculate_gaps(self.start_time_exact, self.stop_time_exact)

    def _get_exact_info(self):
        """ Get exact information, like start and stop time from the header of each file."""
        start_times = []
        stop_times = []
        durations = []
        number_of_channels = []

        for file_path in self.full_paths:
            # Read only file headers
            f = licel.LicelFile(file_path, use_id_as_name=True, import_now=False)
            f.import_header_only()

            # Append properties
            start_times.append(f.start_time)
            stop_times.append(f.stop_time)
            durations.append(f.duration())
            number_of_channels.append(f.number_of_datasets)

        self.start_time_exact = np.array(start_times)
        self.stop_time_exact = np.array(stop_times)
        self.durations = np.array(durations)
        self.number_of_channels = np.array(number_of_channels)

    @staticmethod
    def _calculate_gaps(start_times, stop_times):
        """ Calculate the time difference between consecutive files. .
        """
        gaps = []
        for start_next, stop_previous in zip(start_times[1:], stop_times[:-1]):
            dt = (start_next - stop_previous).total_seconds()
            gaps.append(dt)

        gaps = np.array(gaps)

        return gaps

    @staticmethod
    def get_continuous_idxs(gaps, max_gap_seconds=15):
        """ Calculate continuous indexes.

        Parameters
        ----------
        max_gap_seconds : int
           Maximum number of seconds between files to consider the two files continuous.

        Returns
        -------
        intervals : list
           A list of tuples, containing the start and stop indices for the continuous interval.
        """

        number_of_files = len(gaps) + 1
        large_gap_idxs = np.where(gaps > max_gap_seconds)[0]

        if len(large_gap_idxs) == 0:
            intervals = [(0, number_of_files), ]
        else:
            large_gap_idxs = large_gap_idxs + 1
            initial_idxs = np.insert(large_gap_idxs, 0, 0)
            final_idxs = np.append(large_gap_idxs, number_of_files)

            intervals = [(a, b) for a, b in zip(initial_idxs, final_idxs)]

        return intervals

    def load_measurements(self, start_time=None, stop_time=None, use_id_as_name=True):
        """ Load files covering a specific period. """

        if start_time is None:
            start_time = self.start_time_exact[0]

        if stop_time is None:
            stop_time = self.stop_time_exact[-1]

        # If naive datetimes are provided, assume UTC
        if start_time.tzinfo is None:
            start_time = pytz.utc.localize(start_time)

        if stop_time.tzinfo is None:
            stop_time = pytz.utc.localize(stop_time)

        candidate_idx = (self.start_time_exact >= start_time) & (self.stop_time_exact <= stop_time)
        first_idx = np.argmax(candidate_idx)

        gaps = self._calculate_gaps(self.start_time_exact[candidate_idx], self.stop_time_exact[candidate_idx])
        intervals = self.get_continuous_idxs(gaps)

        # Split the files in continuous intervals
        datasets = []
        for interval_start, interval_stop in intervals:
            idx_min = interval_start + first_idx
            idx_max = interval_stop + first_idx
            files = self.full_paths[idx_min:idx_max]
            current_dataset = {'start_time': interval_start,
                               'stop_time': interval_stop,
                               'measurement': self.measurement_class(files, use_id_as_name=use_id_as_name)}
            datasets.append(current_dataset)

        self.loaded_datasets = datasets

    def plot_volume_depol(self, channel_name_p, channel_name_c, calibration, figsize=(12, 5), vmin=0, vmax=0.5, minimum_length=8, date_labels=False):
        """ Plot a specific channel for a specif interval. """

        # Customize colorbar
        jet = copy.copy(mpl.cm.jet)
        jet.set_over('white')
        jet.set_under('black')
        jet.set_bad('black')

        plt.figure(figsize=figsize)
        ax = plt.subplot(111)
        ax.set_facecolor((0.5, 0.5, 0.5))

        is_first = True

        for dataset in self.loaded_datasets:
            m = dataset['measurement']

            if (channel_name in m.channels.keys()) and (len(m.files) >= minimum_length):
                if is_first:
                    add_colorbar = True
                    is_first = False
                else:
                    add_colorbar = False

                c = m.channels[channel_name]
                c.draw_plot_new(ax, add_colorbar=add_colorbar, vmin=vmin, vmax=vmax, cmap=jet, date_labels=date_labels)

        plt.tight_layout()
        plt.draw()
        plt.show()

    @property
    def number_of_files(self):
        return len(self.full_paths)


class DatalogArchive(LicelArchive):

    def __init__(self, datalog_path, file_pattern, file_version='default', short_filenames=True):
        """ A Licel archive without any Datalog to describe indiviudal datasets.
        Parameters
        ----------
        datalog_path : str
           The path to the datalog file.
        file_pattern : str
           A glob pattern for finding licel files in the archive (e.g. */*/RM*)
        file_version : str
           Version of licel files used. 'default': classic licel file, 'scanning': Scanning file with extra line.
        short_filenames : bool
           If True, short filenames are used (e.g. RM10A0210.351). If False, long filenames are used.
        """
        if not os.path.isfile(datalog_path):
            raise IOError("Datalog path is not a file: {0}".format(datalog_path))

        self.datalog_path = datalog_path
        self.base_path = os.path.dirname(datalog_path)
        self.file_pattern = file_pattern
        self.short_filenames = short_filenames
        self.file_version = file_version
        self.measurement_class = self.measurement_classes[file_version]

        self._find_files_in_archive()

        self.raw_entries = self._read_datalog_entries()
        self.datalog_records = self._get_datalog_records()

    def _read_datalog_entries(self):
        """ Read the datalog file. """
        with open(self.datalog_path, 'r') as f:
            data = f.read()

        total_length = len(data)
        if total_length==0:
            raise IOError("Data log appears to be empty.")

        entries = []

        position = 0
        while position < total_length:
            length_str = data[position:position + 4]
            length = int(codecs.encode(length_str, 'hex'), 16)
            text = data[position + 4:position + 4 + length]
            entries.append(text)
            position += 4 + length

        return entries

    def _get_datalog_records(self):
        """ Convert the list of raw datalog entries to measurement records. """
        # Split the list in groups of 4
        groups = [self.raw_entries[i:i + 4] for i in range(0, len(self.raw_entries), 4)]

        records = []
        for group in groups:
            record = {'user_name': group[0],
                      'location': group[1],
                      'start_date': self._datalog_str_to_date(group[2]),
                      'stop_date': self._datalog_str_to_date(group[3])}

            min_idx = bisect.bisect_left(self.stop_time_approximate, record['start_date'])
            max_idx = bisect.bisect_right(self.stop_time_approximate, record['stop_date'])
            record['files'] = self.full_paths[min_idx:max_idx]

            records.append(record)
        return records

    def print_records(self, min_idx=0, max_idx=None, date_fmt='%Y-%m-%d %H:%M:%S'):
        """ Print a list of records. """

        # Get max length of records
        user_length = max([len(record['user_name']) for record in self.datalog_records[min_idx:max_idx]])
        location_length = max([len(record['location']) for record in self.datalog_records[min_idx:max_idx]])

        print("No  {0:<{user_length}} {1:<{location_length}} Start               Stop".format('User', 'Location', user_length=user_length,
                                                                  location_length=location_length))
        ns = range(len(self.datalog_records))
        for n, record in zip( ns[min_idx:max_idx], self.datalog_records[min_idx:max_idx]):
            user = record['user_name']
            location = record['location']
            start = record['start_date'].strftime(date_fmt)
            stop = record['stop_date'].strftime(date_fmt)
            print("{0:<3} {1:<{user_length}} {2:<{location_length}} {3} {4}".format(n, user, location, start, stop,
                                                                                   user_length=user_length,
                                                                                   location_length=max(location_length, 8)
                                                                                   ))

    def load_measurements(self, record_idxs=None, use_id_as_name=True):
        """ Load files corresponding to the mentioned records.

        Parameters
        ----------
        record_idxs : None or int or list
           Indexes of datalog records. Can be a single index, or a list of indices.
        use_id_as_name : bool
           If True, the transient digitizer name (e.g. BT0) is used as a channel
           name. If False, a more descriptive name is used (e.g. '01064.o_an').
        """
        if type(record_idxs) is int:
            record_idxs = [record_idxs, ]  # Convert to list

        if record_idxs is None:
            record_idxs = range(len(self.datalog_records))

        # Split the files in continuous intervals
        datasets = []

        for idx in record_idxs:
            record = self.datalog_records[idx]
            measurement = self.measurement_class(record['files'], use_id_as_name=use_id_as_name)
            current_dataset = {'idx': idx,
                               'start_time': record['start_date'],
                               'stop_time': record['stop_date'],
                               'measurement': measurement}
            datasets.append(current_dataset)

        self.loaded_datasets = datasets

    def get_measurement(self, record_idx, use_id_as_name=True):
        """ Get the measurement object for a specific record idx.

        Parameters
        ----------
        record_idx : int
           Index of the datalog record to read.
        use_id_as_name : bool
           If True, the transient digitizer name (e.g. BT0) is used as a channel
           name. If False, a more descriptive name is used (e.g. '01064.o_an').
        """
        record = self.datalog_records[record_idx]
        measurement = self.measurement_class(record['files'], use_id_as_name=use_id_as_name)
        return measurement

    def _datalog_str_to_date(self, date_str):
        """ Convert a date string to a datetime object.

        Parameters
        ----------
        date_str : str
           A datetime string in the appropriate format e.g., 00:54:48:34 21/11/2016.

        Returns
        -------
        : datetime object
           The corresponding datetime object.
        """

        if self.short_filenames:
            date_pattern = "%H:%M:%S %d/%m/%Y"
        else:
            date_pattern = "%H:%M:%S:%f %d/%m/%Y"
        return datetime.datetime.strptime(date_str, date_pattern)
